Tiny-VPN
========

Tiny-vpn is a small and basic vpn using a linux kernel driver.

HOW TO BUILD
------------

    cd driver
    make

HOW TO INSTALL
--------------

    cd driver
    sudo make install

HOW TO RUN
----------

If you installed the driver:

    modprobe tiny_vpn

else:
    
    insmod driver/tiny_vpn.ko

Then, on the server side:

    ./user/server.sh -p PORT

On the client side:

   ./user/client.sh -p PORT -a SERVER_ADDR



