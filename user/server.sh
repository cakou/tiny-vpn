#!/bin/sh

NC=nc
PORT=4242
VERBOSE=0

usage ()
{
    echo "Usage: $0 [-p | --port PORT] [-a | --address ADDR] [-v|--verbose]"
    echo "This program run the server part of tiny_vnp"
    echo "Before running it, you must insert tinyvpn.ko"
}

check_driver ()
{
    if ! lsmod | grep tiny_vpn >/dev/null 2>&1; then
        echo "You must insert tiny_vpn.ko before run the script!"
        exit 1
    fi
}

while [ $# -gt 0 ]; do
    case "$1" in
        -p|--port)
            shift
            PORT="$1"
            ;;
        -v|--verbose)
            VERBOSE=1
            ;;
        -h|--help)
            usage
            exit 0
            ;;
        *)
            echo "Bad arg: '$1'."
            usage
            exit 1
            ;;
    esac
    shift
done

check_driver

if [ $VERBOSE -eq 1 ]; then
    cat /dev/tiny_vpn | $NC -l -p $PORT | tee /dev/tiny_vpn | hexdump -C
else
    cat /dev/tiny_vpn | $NC -l -p $PORT > /dev/tiny_vpn
fi

