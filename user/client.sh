#!/bin/sh

NC=nc
PORT=4242
ADDR=127.0.0.1

usage ()
{
    echo "Usage: $0 [-p | --port PORT] [-a | --address ADDR]"
    echo "This program run the client part of tiny_vnp"
    echo "Before running it, you must insert tinyvpn.ko"
}

check_driver ()
{
    if ! lsmod | grep tiny_vpn >/dev/null 2>&1; then
        echo "You must insert tiny_vpn.ko before run the script!"
        exit 1
    fi
}

while [ $# -gt 0 ]; do
    case "$1" in
        -p|--port)
            shift
            PORT="$1"
            ;;
        -a|--address)
            shift
            ADDR="$1"
            ;;
        -h|--help)
            usage
            exit 0
            ;;
        *)
            echo "Bad arg: '$1'."
            usage
            exit 1
            ;;
    esac
    shift
done

check_driver


cat /dev/tiny_vpn | $NC $ADDR $PORT > /dev/tiny_vpn


