#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/wait.h>		/* wait queue */

#include <linux/fs.h>		/* fops */
#include <linux/miscdevice.h>	/* misc driver */

#include <linux/netdevice.h>	/* add a net device */
#include <linux/etherdevice.h>

#include <linux/list.h>         /* used for queue */


/* used to sync on read call */
static DECLARE_WAIT_QUEUE_HEAD(read_wait_queue);

/* used to lock the skb queue */
static DEFINE_SPINLOCK(queue_lock);

/* Misc device handler */
static struct miscdevice	tiny_vpn_misc;

static struct net_device	*tiny_vpn_netdev;

static unsigned long int        g_total = 0;

struct skb_list {
  struct sk_buff        *skb;
  struct list_head      list;
  int                   i;
};

struct skb_list skbs;   /* contain skbs to be send to the reader
                        * We use the list as a queue */


static int is_skb_queue_empty(void)
{
  unsigned long         flags;
  int                   res;

  /* Critical section */
  spin_lock_irqsave(&queue_lock, flags);
  res = list_empty(&skbs.list);
  spin_unlock_irqrestore(&queue_lock, flags);

  return res;
}

static ssize_t tiny_vpn_read(struct file	*file,
			     char		*buf,
			     size_t		count,
			     loff_t		*ppos)
{
  int ret;
  size_t size;
  struct skb_list               *tmp = NULL;
  struct sk_buff		*skb = NULL;
  unsigned long                 flags;

  ret = wait_event_interruptible(read_wait_queue, !is_skb_queue_empty());	// wait on skb
  if (ret < 0) {
    printk(KERN_DEBUG "tiny_vpn: woke up by signal\n");
    return -ERESTARTSYS;
  }

  /* Retreive the skb from the queue */
  if (is_skb_queue_empty())
  {
    printk(KERN_DEBUG "tiny_vpn: try to read on char device but there is nothing to read\n");
    return -ERESTARTSYS;
  }

  /* Critical section */
  spin_lock_irqsave(&queue_lock, flags);
  tmp = list_entry(skbs.list.next, struct skb_list, list);
  skb = tmp->skb;       /* retreive skb */
  list_del(skbs.list.next);
  spin_unlock_irqrestore(&queue_lock, flags);

  kfree(tmp);

  size = min((size_t)skb->len, count);
  if (skb->len > count)
    printk(KERN_INFO "tiny_vpn: W: Coping %u bytes, skb = %u bytes\n",
	   (unsigned int)size, skb->len);
  copy_to_user(buf, skb->data, size);
  dev_kfree_skb(skb);
  return size;
}

static ssize_t tiny_vpn_write(struct file	*file,
			      const char	*buf,
			      size_t		count,
			      loff_t		*ppos)
{
  struct sk_buff *skb;
  struct net_device *dev = tiny_vpn_netdev;

  skb = dev_alloc_skb (count + NET_IP_ALIGN);
  if (skb) {
	skb->dev = dev;
	skb_reserve (skb, NET_IP_ALIGN);
	// Copy data to skb
	//skb_copy_to_linear_data (skb, (void *)buf, count);
	copy_from_user(skb->data, buf, count);	// check skb data max sz.

	// set the size of data
	skb_put (skb, count);
	// Send SKB to stack
	skb->protocol = eth_type_trans (skb, dev);
	netif_rx (skb);
  }
  else
	printk (KERN_WARNING "%s: dev_alloc_skb error\n", __FUNCTION__);

  return count;
}


static int tiny_vpn_open(struct inode *inode, struct file *file)
{
  return 0;
}

static int tiny_vpn_release(struct inode *inode, struct file *file)
{
  return 0;
}



static const struct file_operations tiny_vpn_fops = {
  .owner	= THIS_MODULE,
  .read		= tiny_vpn_read,
  .write	= tiny_vpn_write,
  .open		= tiny_vpn_open,
  .release	= tiny_vpn_release,
};


netdev_tx_t tiny_vpn_start_xmit(struct sk_buff		*skb,
				struct net_device	*dev)
{
  struct skb_list       *tmp = NULL;
  unsigned long         flags;

//  printk(KERN_INFO "tiny_vpn: Transmit. Data len: %u, skb len: %u\n",
//	 skb->data_len, skb->len);

  /* add the skb into the queue */
  tmp = kmalloc(sizeof (struct skb_list), GFP_KERNEL);
  if (tmp == NULL)
    return -ENOMEM;
  tmp->skb = skb;
  tmp->i = g_total++;

  /* Critical section */
  spin_lock_irqsave(&queue_lock, flags);
  list_add_tail(&tmp->list, &skbs.list);
  spin_unlock_irqrestore(&queue_lock, flags);

  wake_up_interruptible(&read_wait_queue);

  return NETDEV_TX_OK;
}

static const struct net_device_ops tiny_vpn_netdev_ops = {
  .ndo_start_xmit	= tiny_vpn_start_xmit,
};


void tiny_vpn_net_setup(struct net_device *dev)
{
  // FIXME: review it.
  /* Fill in device structure with ethernet-generic values. */
  ether_setup(dev);

  /* Initialize the device structure. */
  dev->netdev_ops = &tiny_vpn_netdev_ops;
  dev->destructor = free_netdev;

  /* Interface doesn’t support ARP / MULTICAST */
  dev->flags |= IFF_NOARP;
  dev->flags &= ~IFF_MULTICAST;

  /* set up MAC addr */
  //random_ether_addr(dev->dev_addr);
  strcpy(dev->dev_addr, "tnyvpn");
}




static int __init tiny_vpn_init(void)
{
  int err;

  INIT_LIST_HEAD(&skbs.list);

  /*
   * Init the char driver part.
   */

  tiny_vpn_misc.minor = MISC_DYNAMIC_MINOR;
  tiny_vpn_misc.name = "tiny_vpn";
  tiny_vpn_misc.fops = &tiny_vpn_fops;

  err = misc_register(&tiny_vpn_misc);

  if (err < 0) {
    printk(KERN_WARNING "tiny_vpn: cannot register misc driver\n");
    return err;
  }

  /*
   * Init the net driver part
   */

  tiny_vpn_netdev = alloc_netdev(0, "tiny_vpn%d", tiny_vpn_net_setup);
  if (tiny_vpn_netdev == NULL)
  {
    err = -ENOMEM;
    goto free_misc;
  }

  err = dev_alloc_name(tiny_vpn_netdev, tiny_vpn_netdev->name); // FIXME
  if (err < 0)
    goto free_netdevice;

  printk(KERN_INFO "tiny_vpn: %s addr = %pM\n",
	 tiny_vpn_netdev->name, tiny_vpn_netdev->dev_addr);

  err = register_netdev(tiny_vpn_netdev);
  if (err)
    goto free_netdevice;

  printk(KERN_INFO "tiny_vpn: loaded\n");

  return 0;

free_netdevice:
  free_netdev(tiny_vpn_netdev);
free_misc:
  misc_deregister(&tiny_vpn_misc);
  return err;
}

static void __exit tiny_vpn_exit(void)
{
  misc_deregister(&tiny_vpn_misc);
  unregister_netdev(tiny_vpn_netdev);
  //  free_netdev(tiny_vpn_netdev);
  printk(KERN_INFO "tiny_vpn: unloaded\n");
}


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Camille Lecuyer <camille.lecuyer@gmail.com>");
MODULE_DESCRIPTION("tiny vpn allow user to create easily a basic vpn");



module_init(tiny_vpn_init);
module_exit(tiny_vpn_exit);







